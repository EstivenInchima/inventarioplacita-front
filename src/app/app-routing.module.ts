import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { FruitsComponent } from './components/fruits/fruits.component';
import { FruitsCreateComponent } from './components/fruits/fruits-create/fruits-create.component';
const ROUTES: Routes = [

  { path: '', redirectTo: '/inventario-dashboard', pathMatch: 'full' },
  { path: 'inventario-dashboard', component: DashboardComponent },
  { path: 'fruits', component: FruitsComponent },
  { path: 'fruits-create', component: FruitsCreateComponent }




]
export const ROUTING = RouterModule.forRoot(ROUTES);
