import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FruitDTO } from '../models/fruitDTO';
import { Observable } from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};


@Injectable({
  providedIn: 'root'
})
export class FruitsService {

  constructor(private httpClient: HttpClient) { }


    oFruit: FruitDTO;

    getFruits(): Observable<any> {
      const path = 'http://localhost:8084/inventarioplacita/app/fruits/v1/fruits';
      console.log('ruta en el servicio ', path);
      return this.httpClient.get<any>(path, httpOptions);
    }

    createFruit(oFruit: FruitDTO): Observable<any> {
      const path = 'http://localhost:8084/inventarioplacita/app/fruits/v1/fruit';
      const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
      return this.httpClient.post<any>(path, oFruit, { headers });
    }
    deleteFruit(oFruit: FruitDTO): Observable<any> {
      const path = 'http://localhost:8084/inventarioplacita/app/fruits/v1/fruit/' + oFruit.fruitId;
      const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
      return this.httpClient.delete<any>(path, { headers });
    }

  }

