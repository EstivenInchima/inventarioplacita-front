import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { FruitDTO } from '../../../models/fruitDTO';
import { Router } from '@angular/router';
import { FruitsService } from '../../../service/fruits.service';

@Component({
  selector: 'app-fruits-create',
  templateUrl: './fruits-create.component.html',
  styleUrls: ['./fruits-create.component.scss']
})
export class FruitsCreateComponent implements OnInit {

  form: FormGroup;
  fruit = new FruitDTO();
  submitted = false;
  constructor(
    private router:Router,
    public formBuilder: FormBuilder,
    private fruitService: FruitsService) { }

  ngOnInit() {

    this.form = new FormGroup({
   name: new FormControl('',[Validators.required]),
   description: new FormControl('',[Validators.required]),
   price: new FormControl('',[Validators.required])


    });
  }

  regresar(){
    this.router.navigateByUrl('/fruits');
  }
  save(){
    this.submitted = true;
    if (this.form.valid) {
      const oFruit = this.mapFormToFruit();
      this.fruitService.createFruit(oFruit).subscribe(
        data => {
          alert("Fruta creada correctamente");
          setTimeout(() => { this.regresar(); }, 1600);
        },
        error => {
          // this.vcAlert.generateAlertMessage(AlertClassEnum.DANGER, this.oLanguage.msgError, environment.alertMessageErrorTime);
          alert("OCURRIO UN ERROR AL GUARDAR");

        }
      );
    } else {
      alert('Llene todos los campos obligatorios');
    }
  }

  mapFormToFruit(): FruitDTO {
    this.fruit.fruitName = this.form.controls.name.value;
    this.fruit.fruitDescription = this.form.controls.description.value;
    this.fruit.fruitPrice = this.form.controls.price.value;


    return this.fruit;
  }

  }
