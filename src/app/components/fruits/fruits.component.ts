import { Component, OnInit } from '@angular/core';
import { FruitsService } from '../../service/fruits.service';

import { NavigationExtras, Router, } from '@angular/router';
import { FruitDTO } from 'src/app/models/fruitDTO';

@Component({
  selector: 'app-fruits',
  templateUrl: './fruits.component.html',
  styleUrls: ['./fruits.component.scss']
})
export class FruitsComponent implements OnInit {

  constructor(private fruitService: FruitsService, private router: Router) { }

  oFruitsList: Array<FruitDTO>;
  oFruitSelected: FruitDTO;

  ngOnInit() {
    console.log('en el ng on init')
    this.oFruitsList = new Array<FruitDTO>();
    this.getFruits();
  }
  getFruits(){
      this.fruitService.getFruits().subscribe(
        (data: Array<FruitDTO>) => {
           //console.log('XXXXXXXXXXXXX--->', data);
        this.oFruitsList = data;
        },
        error => {
          console.log('ERRROR');
        }
      );

    }

    eliminar(oFruitSelected: FruitDTO){
     var res =  confirm('esta seguro de eliminar la fruta  ' + oFruitSelected.fruitName);

     if(res == true){

      this.fruitService.deleteFruit(oFruitSelected).subscribe(
        data => {
          alert("Fruta eliminada correctamente");
          this.getFruits();
        },
        error => {
          // this.vcAlert.generateAlertMessage(AlertClassEnum.DANGER, this.oLanguage.msgError, environment.alertMessageErrorTime);
          alert("OCURRIO UN ERROR AL ELIMINAR");

        }
      );
    } else {
      alert("You pressed CANCEL!");
    }

  }
}
