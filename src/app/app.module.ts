import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { NavbarComponent } from './components/navbar/navbar.component';
import { ROUTING } from './app-routing.module';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { FruitsComponent } from './components/fruits/fruits.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { FruitsCreateComponent } from './components/fruits/fruits-create/fruits-create.component';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    DashboardComponent,
    FruitsComponent,
    FruitsCreateComponent
  ],
  imports: [
    BrowserModule,
    MDBBootstrapModule.forRoot(),
    ROUTING,
    FormsModule,
    HttpClientModule,
    CommonModule,
    ReactiveFormsModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
